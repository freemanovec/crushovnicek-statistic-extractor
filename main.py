from telethon.sync import TelegramClient
from telethon.tl.functions.messages import SendVoteRequest
from telethon.tl.types import MessageMediaPoll
from dotenv import load_dotenv
from os import getenv
import csv, sys
import datetime

def main():
    load_dotenv()
    api_id = getenv('API_ID')
    api_hash = getenv('API_HASH')
    channel_username = "crushefursczsk"

    with TelegramClient("crushed", api_id, api_hash) as client:
        # first vote no in all unvoted polls
        for message in client.iter_messages(channel_username, reverse = True):
            if message.media is not None:
                media = message.media
                if isinstance(media, MessageMediaPoll) and media.poll is not None and media.results is not None:
                    poll = media.poll
                    results = media.results
                    title = poll.question
                    if results.results is not None:
                        # vote has already been cast, skip
                        print(f"#Vote already cast for '{title}'")
                        continue
                    # find the id of the "No" answer
                    answers = poll.answers
                    no_id = None
                    for i in range(len(answers)):
                        answer = answers[i]
                        text = str(answer.text).lower()
                        if len(text) >= 2:
                            text = text[:2]
                        if text in ["ne", "no", "n", "ni"] and "neznám" not in text:
                            no_id = answer.option
                            break
                    if no_id is None:
                        # no negative answer found, skip
                        print(f"#No negative answer found for '{title}'")
                        continue
                    result = client(SendVoteRequest(
                        channel_username,
                        message.id,
                        [no_id]
                    ))
                    print(f"#Cast negative vote on '{title}'")

        dataset = []
        # then analyze all polls and get names and ratios
        ix = 0
        origin_time = None
        for message in client.iter_messages(channel_username, reverse = True):
            if message.media is not None:
                media = message.media
                if isinstance(media, MessageMediaPoll) and media.poll is not None and media.results is not None:
                    poll = media.poll
                    results = media.results.to_dict()['results']
                    answers = poll.answers
                    title = poll.question
                    if len(results) == 0:
                        print(f"#No results for '{title}'")
                        continue
                    if origin_time is None:
                        origin_time = message.date
                    votes = [(answers[i].text, results[i]['voters']) for i in range(len(answers))]

                    # find positive and negative votes
                    positive_votes = None
                    negative_votes = None
                    total_votes = 0
                    for vote in votes:
                        text = vote[0].lower()
                        if len(text) > 2:
                            text = text[:2]
                        count = vote[1]
                        if text in ["an", "ye", "y", "da"]:
                            if positive_votes is None:
                                positive_votes = count
                            else:
                                positive_votes += count
                        elif text in ["ne", "no", "n", "ni"] and "neznám" not in text:
                            if negative_votes is None:
                                negative_votes = count
                            else:
                                negative_votes += count
                        total_votes += count
                    if positive_votes is None or negative_votes is None:
                        print(f"#Insufficient data for '{title}'")
                        continue

                    if positive_votes + negative_votes > 0:
                        ratio = positive_votes / (positive_votes + negative_votes) * 100
                    else:
                        print(f"#Not enough votes for '{title}'")

                    prefix = "Máte crush na: "
                    if len(title) <= len(prefix) + 1:
                        print(f"#Unparsable poll title for '{title}'")
                        continue

                    username = title[len(prefix):]
                    if(username[-1] == "?"):
                        username = username[:-1]
                    if "? @" in username:
                        username = username.replace("? @", " @")

                    since_first = message.date - origin_time

                    line = [
                        ix,
                        username,
                        positive_votes,
                        negative_votes,
                        positive_votes + negative_votes,
                        total_votes,
                        int(since_first.total_seconds())
                    ]
                    dataset.append(line)

                    #dataset.append((username, positive_votes, positive_votes + negative_votes))

                    print(f"#{username} => {positive_votes} positive votes")
                    ix += 1

        dataset.sort(key=lambda x: x[2], reverse = True)

        print("#--- CRUSHABILITY CHARTS ---")
        for i in range(len(dataset)):
            place = i + 1
            username = dataset[i][1]
            crushability = dataset[i][2]
            total_votes = dataset[i][4]
            fmt_place = "#" + str(place).ljust(4)
            fmt_crushability = crushability
            print(f"#{fmt_place} - {fmt_crushability} ano - {username} ({total_votes} hlasů)")
        print("#-- BEGIN CSV --")
        print("INDEX,USERNAME,POSITIVE_VOTES,NEGATIVE_VOTES,MEANINGFUL_VOTES,TOTAL_VOTES,SECONDS_SINCE_FIRST_POLL")
        dataset.sort(key=lambda x: x[0], reverse = False)
        writer = csv.writer(sys.stdout)
        writer.writerows(dataset)
        print("#--  END CSV  --")
        


if __name__ == "__main__":
    main()
